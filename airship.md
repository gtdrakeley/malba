# Table of Contents

[[_TOC_]]

# Airship Orb Mechanics

## Metal Orb
**Activate** ◇ manipulate; **Frequency** once per round; **Effect** Take a single crew action.

**Activate** ◆◆ manipulate, envision; **Frequency** once per day; **Effect** You may enhance one consumable to the next tier of effectiveness. For example you could convert a moderate [*elixir of life*](https://2e.aonprd.com/Equipment.aspx?ID=91) to a greater [*elixir of life*](https://2e.aonprd.com/Equipment.aspx?ID=91).

## Wood Orb
**Activate** ◆ manipulate; **Effect** The vines, trunks, and plants comprising the body of the Wood Giant grow and reshape to surround the airship with the construct armor. The airship gains a +2 item bonus to AC and +5 item bonus to hardness.

**Activate** ◆ manipulate; **Effect** The vines, trunks, and plants comprising the body of the Wood Giant grow and reshape to proect the airship from a single direction. Pick up, down, left, or right. The airship gains a +4 item bonus to AC and a +10 item bonus to hardness against attacks from the chosen direction.

**Activate** ◆◆◆ manipulate; **Effect** The Wood Giant's body releases itself from the airship and reshapes itself into the form of the Wood Giant

**Activate** ◆◆◆ manipulate; **Effect** The Wood Giant's body reforms around the airship to provide protection, growing around it like a suit of armor. When you use this ability, you may may choose which version of protection the airship gains from the abilities above.

## Air Orb
**Activate** ◇ manipulate; **Effect** The air orb shunts the airship in a direction with a burst of air. Take a move action, this move action can move the airship in any direction regardless of heading and does not change the heading of the airship. You may not use this ability again for 1d4 rounds.

## Fire Orb
**Activate** ◆◆ manipulate, envision; **Frequency** three times per day; **Effect** The airship releases 3 illusory copies as the spell [*mirror image*](https://2e.aonprd.com/Spells.aspx?ID=197).

**Activate** ◆◆ manipulate, envision; **Frequency** once per day; **Effect** The airship disappears from sight as the spell [*invisibility*](https://2e.aonprd.com/Spells.aspx?ID=164)

## Water Orb
Controls the submarine functionality of the airship. Produces air for use while underwater.
