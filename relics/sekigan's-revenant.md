# Sekigan's Revenant
This cloak belongs to the barbarian Tolen.

[[_TOC_]]

## Base Item

[Coyote Cloak](https://2e.aonprd.com/Equipment.aspx?ID=426)

## Aspects
- Animal
- Fiend

## Gifts

### Minor

#### [1] Beast Senses *(Animal)*

Ref: https://2e.aonprd.com/Relics.aspx?ID=7

#### [5] Fiendish Fervor *(Fiend)*

**Activate** ◇ envision **Trigger** You roll initiative; **Frequency** once per day; **Effect** You rage.

### Major

#### [9] Baleful Howl *(Animal)*
**Activate** ◆◆ envision, verbal **Frequency** once per day; **Area** 10 ft emanation; **Saving Throw** Will; **Effect** You emit a ferocious howl. All enemies in range make a Will save with the following effects:

**Critical Success** Unaffected.\
**Success** Frightened 1.\
**Failure** Frightened 2.\
**Critical Failures** Frightened 2 and paralyzed for 1 round.

**Special** At level 12 the area increases to a 15 ft emanation. At level 15 you may use Baleful Howl an additional time per day. At level 18 you increase the value of the frightened condition by 1 on a success or failure, and on a critical failure you increase the frightened condition by 1 and the enemy cowers in fear and Drops Prone.

#### [13] Fiendish Bargain *(Fiend)*

Ref: https://2e.aonprd.com/Relics.aspx?ID=34

### Grand

#### [17] Mantle of the Coyote Demon *(Fiend)*

**Activate** ◆◆ envision, interact; **Frequency** once per day; **Requirements** You are raging; **Duration** 1 minute; **Effect** Pulling the cloak tight around your body, it envelops you like a second skin. The cloak's fur lengthens as it suffuses you with demonic energies. Your teeth grow long and sharp, your eyes turn a crimson red, and your features become evocative of a hellish canine. You heal 25 hp and gain fast healing 15. Your enraged strikes are critical hits on a 19 on the die as long as that result is a success. If you already critically hit on a 19, instead on critical hits your weapon rends the soul of the target inflicting drained 1. This effect is cumulative with any other drained conditions the target has. You may use the Protect the Pack reaction once during the duration. If you stop raging, Mantle of the Coyote Demon ends.

**Protect the Pack** ↻ **Trigger** An enemy within 60 ft of you hits an ally; **Effect** Sekigan's Mantle urges you forward in fiendish retribution. As you move, enormous paw prints appear along your path and a faint aura in the shape of an enormous, infernal coyote flickers in and out of existance with you at its center. You stride up to 60 ft to the nearest adjacent square to the triggering enemy that has room for your character. You may Strike the enemy. If the strike is a critical hit, you may choose to push the enemy back 10 ft or knock them prone.

**Special** For every level above 17, the amount you heal on activation increases by 5. At level 20 the fast healing increases to 20 and you may use Mantle of the Coyote Demon an additional time per day.
